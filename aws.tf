provider "aws" {
  version = "~> 2.0"
  profile = "prunux-flashlight-dev-aws-terraform"
  region  = "eu-west-1"
}