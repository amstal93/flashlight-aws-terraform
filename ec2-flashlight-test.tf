resource "aws_key_pair" "flashlight" {
  key_name   = "flashlight-ssh-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDS9xdtCXvEEA/ueBgsnhb0yzt4U3yFW5KunIAw8obxNfBjWQsjFhJaF8pSsxRFFgNP+x3VW+3CpOR7EKusAFsImvBaStcHJcUZ2MmlrM1meklwLbw69AvPGgG61kZe5fDnxgs6l6F7RKVBaZniiN6tyNTdOnjvrgubAr8+x1EwoVMfKxWE28X0vilkB3Hdwn+t5XsVSeyPpIOr+sTnTx1t8QxJs5y7AcpYz9naibNt9ETrL9rJ6g0kaGWKdA+cUlXF+pJEYxRlrAZxjdxO+KfXcUieC7tSuURA2DqrR70OV7PyX7zbg1/bGLT8tGX0jgcaw5YFYjs/6WAifrr99q++jphhgmGodQrdZk5DzxveA2FFR25uDx2fSzrZNT5/14t8nwiTbdLWPSMMh/J7sY6rUOdh2Ux5tHa7dE/pvnDC3cIiY2cT0kXFhQQYFUYrgqfhw8p7QPyNf8i25OKETz5vE/5uynOq/CydFv4R3Tnx7jCW+l2olGWeVUcaPiDUmFFRw+2QarpteUVbudVNVaabjhsdcfqzyumXc5IYPIYz/D96ZXS3Qdc7+1cC/WysMIGYpcTrfUOMBz9E/1UPfOHz0j9MzZhT5Puu1MiiYLXNxQR5hsSrka5imlbwi31Vc4QUQJT8cDx5yumQn0NIRyi6SFjAOfN6Ru+QnYj3+jSWeQ== rplessl@flashlight.test.prunux.ch"
}

resource "aws_instance" "flashlight" {
  count = 1
  ami   = data.aws_ami.ubuntu-bionic-amd64.id

  instance_type = "t3a.nano"
  credit_specification {
    cpu_credits = "standard"
  }

  subnet_id = module.vpc.private_subnets[count.index % 3]
  vpc_security_group_ids = [
    module.vpc.default_security_group_id,
  ]

  ebs_optimized = true
  root_block_device {
    delete_on_termination = true
    encrypted             = true
  }
  volume_tags = {
    Environment = "flashlight-test"
    Terraform   = true
  }

  disable_api_termination = false
  monitoring              = false

  tags = {
    Name        = "flashlight-test"
    Terraform   = true
    Environment = "flashlight-test"
    Flashlight  = "enabled" # see gitlab.com/prunux/flashlight-aws-terraform
  }
}